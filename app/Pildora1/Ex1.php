<!DOCTYPE html>
<html>
    /*Ex 1: Crea una pagina en la que se almacenan dos numeros en variables 
    y muestre en una tabla los valores que toman las variables y cada uno de 
    los resultados de todas las operaciones aritmeticas.*/
    <head>
        <title>Ejercicio 1</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    </head>
    <body>
        <?php
        $numero1 = 7;
        $numero2 = 2;
        $suma = $numero1 * $numero2;
        $resta = $numero1 - $numero2;
        $multiplicacio = $numero1 * $numero2;
        $divisio = $numero1 / $numero2;
        $exponent = $numero1 ** $numero2;
        ?>
        <table class="table table-striped table-hover">
            <tr>
                <th>Operacion</th>
                <th>Valor</th>
            </tr>
            <tr>
                <td>A</td>
                <td><?php echo $numero1 ?></td>
            </tr>
            <tr>
                <td>B</td>
                <td><?php echo $numero2 ?></td>
            </tr>
            <tr>
                <td>A + B</td>
                <td><?php echo $suma ?></td>
            </tr>
            <tr>
                <td>A - B</td>
                <td><?php echo $resta ?></td>
            </tr>
            <tr>
                <td>A * B</td>
                <td><?php echo $multiplicacio ?></td>
            </tr>
            <tr>
                <td>A / B</td>
                <td><?php echo $divisio ?></td>
            </tr>
            <tr>
                <td>A exp B</td>
                <td><?php echo $exponent ?></td>
            </tr>
        </table>
    </body>
</html>