<?php 
include_once('_header.php');
include('UploadClass.php');
/*
* Función que se encarga de subir un archivo y moverlo a la carpeta /pictures
* que almacena todas las fotos.
* Return: Devuelve la ruta final del archivo.
*/


$nom = $_POST["title"];
$file = $_POST["foto"];
$filename = $_FILES["foto"]["name"];

$penjarFoto = new Upload();
$penjarFoto->uploadPicture($file, $nom, $filename);
/*function uploadPicture(){
    try {
        
        $nom = $_POST["title"];
        $file = $_POST["foto"];
        $filename = $_FILES["foto"]["name"];
        
        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        if (empty($nom)) {
            throw new Exception('Error: Falta poner el titulo');
        } else if (file_exists($file)) {
            throw new Exception('Error: El archivo no existe');
        } else if ($extension != "jpg" && $extension != "jpeg" && $extension != "gif" && $extension != "png") {
            throw new Exception('Error: El archivo no es una imagen');
        } else if (file_exists("./fotos/" . $filename)) {
            throw new Exception('Error: Ya existe un archivo con ese nombre');
        } else {
            $file = fopen($this->path, "r");
            $i = 0;
            while(!feof($file)){
                $line = fgets($file);
                $missatge = substr($line, 0, strpos($line, "###"));
                if ($missatge == $nom) {
                    throw new Exception('Error: Ya existe un archivo con ese titulo');
                }
            }

            move_uploaded_file($_FILES["foto"]["tmp_name"], "./fotos/" . $filename);
            addPictureToFile($filename, $nom);

            header("Location: index.php?upload=success");
        }

        
        


    } catch (Exception $e) {
        header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));
    }
    return "./fotos/" . $filename;
}

/*
* Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
* fotografía recien subida
* Entradas:
*       $file_uploaded: La ruta del archivo
*       $title_uploaded: El titulo del archivo
* Return: null
*/
/*function addPictureToFile($file_uploaded,$title_uploaded){
    if (file_exists("./fotos.txt")){
        $fotos = fopen("./fotos.txt", "a");
        fwrite($fotos, $title_uploaded . "###" . "./fotos/" . $file_uploaded . "\n");
        fclose($fotos);
    } else {
        $fotos = fopen("./fotos.txt", "w");
        fwrite($fotos, $title_uploaded . "###" . "./fotos/" . $file_uploaded . "\n");
        fclose($fotos);
    }
}*/

/*
* Clase personalizada extendida de Exception que utilizaremos para lanzar errores
* en la subida de archivos. Por ejemplo:
* throw new UploadError("Error: Please select a valid file format.");
*/
/*class UploadError extends Exception{}*/

include_once('_footer.php');
?>