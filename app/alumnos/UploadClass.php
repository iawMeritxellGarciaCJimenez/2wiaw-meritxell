<?php 

class UploadError extends Exception{}

class Upload{

    private $name;

    function --constructor($name)
    {
        $this->name=$name;
        if(isset($_FILE[$name])) $this->upload();
    }

    public function upload()
    {
        try{
        $filename = $_FILES[$this->name]["name"];

        if(file_exists( RUTA . $filename))
            throw new UploadError("Ese archivo ya existe");  //És per a dir que ho mostri com error encara que no sigui error del sistema
        move_uploaded_files($_FILES[$this->name]["tmp_name"], RUTA . $filename);
        }catch (UploadError $e){
            echo "Error de subida: " . $e->getMessage();
        }catch (Exception $e){  //Errors del sistema ( indicat amb el Exception )
            echo $e->getMessage();
        }

    }

    public function getPath(){}

}
?>