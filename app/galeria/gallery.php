<!DOCTYPE html>
<html>
    <body>
        <?php 
        include_once('GalleryClass.php');
        include_once('PictureClass.php');
        include_once('_header.php');
        $gallery = new Gallery("./fotos.txt");
        $fotos = $gallery->getGallery();
        foreach($fotos as $valor) {
        ?>
        <div class="container-md" style="width: 700px;">
            <img src="<?=$valor->fileName()?>" class=" img-thumbnail">
            <p><?=$valor->title()?></p>
        </div>
        <?php
        }
        include_once('_footer.php') ?>
    </body>
</html>