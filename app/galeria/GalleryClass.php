<?php 
include('PictureClass.php');
class Gallery {
   private $path;
   private $_gallery = [];

  /*Constructor: Recibe la ruta del archivo fotos.txt*/
    function __construct($fileName){
      //if (isset($fileName)) {
        $this->path = $fileName;
        $this->loadGallery();
      /*} else {
        echo "hola";
      }*/
    }

    /*
  *Recorre el archivo fotos.txt y para cada linea, crea un
  *elemento Picture que lo añade al atributo $_gallery[]
  */
    function loadGallery(){
      $file = fopen($this->path, "r");
      $i = 0;
      while(!feof($file)){
        $line = fgets($file);
        $missatge = substr($line, 0, strpos($line, "###"));
        $foto = substr($line, strpos($line, "###") + 3);
        $picture = new Picture($missatge, $foto);
        $this->_gallery[$i] = $picture;
        $i++;
      }
      fclose($file);
    }

    /*
  *Getters.
  */
  public function getGallery(){
    return $this->_gallery;
  }

  public function getPath(){
    return $this->path;
  }
}
?>


