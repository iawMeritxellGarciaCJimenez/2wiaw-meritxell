<?php
class Picture
{
  private $_title;
  private $_fileName;

    /*Constructor*/
    function __construct($title, $fileName){
      $this->_title = $title;
      $this->_fileName = $fileName;
    }

    /*
  *Getters. Lo que quiere decir que los atributos de
  *title y filename son private
  */
    public function title(){
      return $this->_title;
    }
    public function fileName(){
      return $this->_fileName;
    }
}
?>