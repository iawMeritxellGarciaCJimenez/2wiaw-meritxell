<!DOCTYPE html>
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    </head>
    <body>
	    <div class="p-2 bg-dark text-white container-fluid">
		<h3>Image's Gallery</h3>
	    </div>

        <div class="container mt-3">
            <?
            if ($_GET["upload"] == 'success') { ?>
                <div class="alert alert-success" role="alert">
                    Picture Uploaded 
                </div>
            <? 
            } 
            if ($_GET["upload"] == 'error') { ?>
                <div class="alert alert-danger" role="alert">
                    <?=$_GET["msg"]?>
                </div>
            <? } ?>
        </div>

