<!DOCTYPE html>
<html>
    <head>

        <title>Datos</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

        <h1>Datos</h1>
    </head>
    <body>
        <?php
            $noms = $_POST["noms"];
            $gols = $_POST["gols"];
        ?>
        <table class="table table-striped">
                <tr>
                    <th scope="col">Jugador</th>
                    <?php
                        $partits = $_POST["partits"];
                        $jugadors = $_POST["jugadors"];
                        for($i = 1; $i <= $partits; $i++){?>
                            <th scope="col">Partido <?=$i?></th>
                        <?}?>
                </tr>
                <?php
                    for($i = 0; $i < $jugadors; $i++){?>
                        <tr><td><?=$noms[$i]?></td>
                        <?for($j = 0; $j <= $partits; $j++){?>
                            <td><?=$gols[$i][$j]?></td>
                        <?}?>
                        </tr>
                    <?}?>
            </table>
    </body>
</html>