<?php 
class Persona
{
    //nom alumne
    private $name;
    //cognom de l'alumne
    private $surname;
    //fotografia ( Upload file )
    private $picture;
    //adreça alumne
    private $address;
    private $comment;
    private $math;

    public function setName($name)
    {
        $this->name = $name;
    }
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }
    public function setAddress($address)
    {
        $this->address = $address;
    }


    public function getName()
    {
        return $this->name ;
    }
    public function getSurname()
    {
        return $this->surname ;
    }
    public function getPicture()
    {
        return $this->picture ;
    }
    public function getAddress()
    {
        return $this->address ;
    }

}
?>