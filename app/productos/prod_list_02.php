<!DOCTYPE html>
<html>
    <head>

        <title>Product's List</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

        <h1>Product's List</h1>
    </head>
    <body>
        <form method="post" action="prod_list_03.php" >
            <table>
                <?php
                    $numprod = $_POST["numprod"];
                    for($i = 0; $i < $numprod; $i++){?>
                        <tr><td>Product Name</td><td><input type="text" name="nameprod[<?=$i?>]" /></td>
                        <td>Price</td><td><input type="text" name="price[<?=$i?>]" /></td></tr>
                    <?}?>
                    <input type="hidden" name="numprod" value="<?=$numprod?>"/>
            </table>
            <input type="submit" value="Send" />
            
        </form>
    </body>
</html>