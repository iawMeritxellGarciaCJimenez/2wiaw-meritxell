<!DOCTYPE html>
<html>
    <head>

        <title>Product's List</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

        <h1>Product's List</h1>
    </head>
    <body>
        <?php
            $nameprod = $_POST["nameprod"];
            $price = $_POST["price"];
            $numprod = $_POST["numprod"];
        ?>
        <table class="table table-striped">
                <tr>
                    <th scope="col">Product Name</th>
                    <th scope="col">Price</th>
                </tr>
                    <?php
                        for($i = 0; $i < $numprod; $i++){
                            if($nameprod[$i] == "" && $price[$i] == ""){?>
                                <tr><td>Product not inserted</td></tr>
                            <?} else {?>
                                <tr><td><?=$nameprod[$i]?></td><td><?=$price[$i]?></td></tr>
                            <?}
                        }
                    ?>
            </table>
    </body>
</html>