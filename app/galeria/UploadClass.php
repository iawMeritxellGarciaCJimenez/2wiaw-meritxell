<?php
define('FOLDER','/POO/Practica Entrega/fotos/');
class UploadError extends Exception{}

class Upload {

    function __construct() {
    }

    function uploadPicture($file, $nom, $filename){
        try {
            $uploadFolder=$_SERVER['DOCUMENT_ROOT'].FOLDER;
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            if (empty($nom)) {
                throw new UploadError('Error: Falta poner el titulo');
            } 
            if (file_exists($file)) {
                throw new UploadError('Error: El archivo no existe');
            } 
            if ($extension != "jpg" && $extension != "jpeg" && $extension != "gif" && $extension != "png") {
                throw new UploadError('Error: El archivo no es una imagen');
            } 
            if (file_exists($uploadFolder . $filename)) {
                throw new UploadError('Error: Ya existe un archivo con ese nombre');
            } 
            
                $file = fopen("./fotos.txt", "r");
                $i = 0;
                while(!feof($file)){
                    $line = fgets($file);
                    $missatge = substr($line, 0, strpos($line, "###"));
                    if ($missatge == $nom) {
                        throw new UploadError('Error: Ya existe un archivo con ese titulo');
                    }
                }
                if(!is_writable( $uploadFolder))
                    throw new UploadError('Error:No tienes permisos');

                move_uploaded_file($_FILES["foto"]["tmp_name"], $uploadFolder . $filename);
                $this->addPictureToFile($filename, $nom);
         
                header("Location: index.php?upload=success");


        } catch (UploadError $e) {
            header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));
        } catch (Exception $e) {
            header('Location: index.php?upload=error&msg=' . urlencode($e->getMessage()));
        }
        return "./fotos/" . $filename;
    }
    
    /*
    * Función que se encarga de añadir al archivo fotos.txt el titulo y la ruta de la
    * fotografía recien subida
    * Entradas:
    *       $file_uploaded: La ruta del archivo
    *       $title_uploaded: El titulo del archivo
    * Return: null
    */
    function addPictureToFile($file_uploaded,$title_uploaded){
        if (file_exists("./fotos.txt")){
            $fotos = fopen("./fotos.txt", "a");
            fwrite($fotos, $title_uploaded . "###" . "./fotos/" . $file_uploaded . "\n");
            fclose($fotos);
        } else {
            $fotos = fopen("./fotos.txt", "w");
            fwrite($fotos, $title_uploaded . "###" . "./fotos/" . $file_uploaded . "\n");
            fclose($fotos);
        }
    }
    
    /*
    * Clase personalizada extendida de Exception que utilizaremos para lanzar errores
    * en la subida de archivos. Por ejemplo:
    * throw new UploadError("Error: Please select a valid file format.");
    */

}
?>