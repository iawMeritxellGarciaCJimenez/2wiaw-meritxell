<!DOCTYPE html>
<html>
    <body>
        <?php include_once('_header.php') ?>
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <h1>Upload Picture</h1>
                    <form method="post" action="uploadManager.php" enctype="multipart/form-data">
                        <label for="title">Title:</label><br>
                        <input type="text" name="title" ><br><br>
                        <label for="file">Picture:</label><br>

                        <input type="file" name="foto" if="file" class="form-control"><br><br>
                        <input type="submit" value="Upload" name="upload" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
        <?php include_once('_footer.php') ?>
    </body>
</html>