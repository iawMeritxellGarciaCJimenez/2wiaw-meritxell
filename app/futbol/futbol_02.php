<!DOCTYPE html>
<html>
    <head>

        <title>Nombres y goles</title>

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

        <h1>Nombres y goles</h1>
    </head>
    <body>
        <form method="post" action="futbol_03.php" >
            <table>
                <tr>
                    <td>Nombre jugador</td>
                    <?php
                        $partits = $_POST["partits"];
                        $jugadors = $_POST["jugadors"];
                        for($i = 1; $i <= $partits; $i++){?>
                            <td>Partido <?=$i?>: Goles</td>
                        <? }
                    ?>
                </tr>
                <?php
                    for($i = 0; $i < $jugadors; $i++){?>
                        <tr><td><input type="text" name="noms[<?=$i?>]" /></td>
                       <? for($j = 0; $j < $partits; $j++){?>
                            <td><input type="int" name="gols[<?=$i?>][<?=$j?>]" /></td>
                        <?}?>
                        </tr>
                    <?}?>
                    <input type="hidden" name="partits" value="<?=$partits?>"/>
                    <input type="hidden" name="jugadors" value="<?=$jugadors?>"/>
            </table>
            <input type="submit" value="Enviar" />
        </form>
    </body>
</html>